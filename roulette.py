import random

print("====================ROULETTE====================")
compte = 100
continuer = True
num_partie = 0
parties = list()


def num_valide(numero):
    if numero == "":
        numero = "0"
    numero = int(numero)
    if numero < 0:
        numero = abs(numero)
    if numero == 0:
        numero = 1
    if numero > 50:
        numero = 50
    return numero


def mise_valide(mise):
    if mise == "":
        mise = "10"
    mise = int(mise)
    if mise < 0:
        mise = abs(mise)
    if mise == 0:
        mise = 10
    if mise > compte:
        mise = compte
    return mise


while continuer:
    num_partie += 1
    print("___________________Jeu ", num_partie, "___________________")
    num_a_valider = input("Quel numéro? [1 à 50]: ")
    num = num_valide(num_a_valider)
    combien_a_valider = input("Combien? [10/" + str(compte)+"]")
    combien = mise_valide(combien_a_valider)
    tirage = random.randint(1, 50)
    print("Roule ma poule........", combien,
          " sur le ", num, ".....:", tirage)

    if tirage == num:
        combien *= 3
        print("oh oH OH! tu gagnes ", combien)
        compte += combien
    elif (tirage % 2) == (num % 2):
        combien = int(combien/2)
        print(":-) meme couleur! tu recuperes ", combien)
        compte += combien
    else:
        print("Rien ici, tu perds ", combien)
        compte -= combien

    if compte > 0:
        rejouer = input("Tu rejoues? (solde: " + str(compte)+") [Y/n]")
        if rejouer == "n":
            continuer = False
            print("A bientot l'ami.")
    else:
        print("Fini de jouer, ton compte est à plat ")
        break
