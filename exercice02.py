def square(number):
    # Votre code ici
    return number * number


def run():
    assert square(1) == 1
    assert square(2) == 4
    assert square(3) == 9
    assert square(23) == 529
    assert square(-23) == 529


# chiffre_en_lettres = input("Choisi un chiffre: ")
# chiffre = int(chiffre_en_lettres)
# carre = square(chiffre)
# print("Son carré est", carre)
