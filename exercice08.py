import string


def get_word_count(sentence):
    # Votre code ici
    nb_mot = 0
    for mot in sentence.split():
        est_lettre = False
        for lettre in mot:
            if lettre in string.ascii_letters:
                est_lettre = True
                break
        if est_lettre:
            nb_mot += 1
    return nb_mot


def run():
    assert get_word_count("Bonjour") == 1
    assert get_word_count("Bonjour toi") == 2
    assert get_word_count("Bonjour ca va ?") == 3
    assert get_word_count("Bonjour ca va toi ?!") == 4
    assert get_word_count("") == 0
