def get_category(age):
    # Votre code ici
    if age >= 12:
        return "Cadet"
    elif age >= 10:
        return "Minime"
    elif age >= 8:
        return "Pupille"
    elif age > 5:
        return "Poussin"
    else:
        raise ValueError("attends de savoir marcher")


def run():
    assert get_category(6) == "Poussin"
    assert get_category(7) == "Poussin"
    assert get_category(8) == "Pupille"
    assert get_category(9) == "Pupille"
    assert get_category(10) == "Minime"
    assert get_category(11) == "Minime"
    assert get_category(12) == "Cadet"
    assert get_category(99) == "Cadet"

    try:
        get_category(1)
        raise AssertionError()
    except ValueError:
        pass
