def factorial(number):
    # Votre code ici
    fact = abs(number)
    for i in range(1, abs(number)):
        fact *= i
    if number < 0:
        return -fact
    return fact


def run():
    assert factorial(1) == 1
    assert factorial(2) == 2
    assert factorial(3) == 6
    assert factorial(4) == 24
    assert factorial(8) == 40320
    assert factorial(-8) == -40320
