import string


def get_letter_count(word):
    # Votre code ici
    compteur = 0
    for lettre in word:
        if lettre in string.ascii_letters:
            compteur += 1
    return compteur


def run():
    assert get_letter_count("Oui") == 3
    assert get_letter_count("Bonjour") == 7
    assert get_letter_count("") == 0
    assert get_letter_count(".........hein???") == 4
    assert get_letter_count("Attention y'a quatre mots !") == 21
